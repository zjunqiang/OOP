#include <iostream>
using namespace std;

class CDocument
{
public:
    void OnFileOpen()
    {
        cout << "dialog..." << endl;
        cout << "check file status..." << endl;
        cout << "open file..." << endl;

        Serialize();

        cout << "close file..." << endl;
        cout << "update all views..." << endl;
    }

    virtual void Serialize()
    {
    }
};

class CMyDoc: public CDocument
{
public:
    virtual void Serialize()
    {
        //只用应用程序本身才知道如何读写自己的文件
        cout << "CMyDoc::Serialize()..." << endl;
    }
};

class CMyExcel: public CDocument
{
public:
    virtual void Serialize()
    {
        //只用应用程序本身才知道如何读写自己的文件
        cout << "CMyExcel::Serialize()..." << endl;
    }
};

int main()
{
    CMyDoc myDoc;
    myDoc.OnFileOpen();

    cout << "---------------------" << endl;

    CMyExcel myExcel;
    myExcel.OnFileOpen();

    cout << "---------------------" << endl;

    CDocument *p;

    p = &myDoc;
    p->OnFileOpen();

    cout << "---------------------" << endl;

    p = &myExcel;
    p->OnFileOpen();

    return 0;
}
