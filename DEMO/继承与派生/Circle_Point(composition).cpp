//Circle类 & Point类(composition)

#include <iostream>
using namespace std;

//class Point
class Point
{
public:
    Point(double = 0, double =  0);     //constructor
    Point(const Point &);               //copy constructor
    friend ostream& operator<<(ostream& output, const Point &p);

private:
    double x;
    double y;
};

//constructor
Point::Point(double x, double y):x(x), y(y)
{
    cout << "Point constructor called" << endl;
}

//copy constructor
Point::Point(const Point &p)
{
    this->x = p.x;
    this->y = p.y;
    cout << "Point copy constructor called" << endl;
}

ostream& operator<<(ostream& output, const Point &p)
{
    output << '(' << p.x << ", " << p.y << ')';
    return output;
}

//class Circle
class Circle
{
public:
    Circle(const Point &, double);
    friend ostream& operator<<(ostream& output, const Circle &p);
private:
    Point centerPoint;      // composition: member object
    double radius;
};

Circle::Circle(const Point &p, double radius):centerPoint(p), radius(radius)
{
    cout << "Circle constructor called" << endl;
}

ostream& operator<<(ostream& output, const Circle &c)
{
    output << "Center:" << c.centerPoint;
    output << "\nRadius:" << c.radius;
    return output;
}

int main()
{
    Point p1(1, 2);
    cout << p1 << endl;

    Circle c(p1, 100);

    cout << c << endl;
    return 0;
}
