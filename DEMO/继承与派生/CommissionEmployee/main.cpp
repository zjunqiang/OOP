/*
//Fig. 12.6: fig12_06.cpp
#include <iostream>
using std::cout;
using std::endl;
using std::fixed;

#include <iomanip>
using std::setprecision;

#include "CommissionEmployee.h"

int main()
{
    CommissionEmployee employee("Sue", "Jones", "222-22-2222", 10000, 0.06);

    // set foating-point output formatting
    cout << fixed << setprecision(2);

    // get commission employee data
    cout << "Employee information obtained by get functions:\n"
         << "\nFirst name is " << employee.getFirstName()
         << "\nLast name is" << employee.getLastName()
         << "\nSocial security number is " << employee.getSocialSecurityNumber()
         << "\nGross sales is " << employee.getGrossSales()
         << "\nCommission rate is " << employee.getCommissionRate() << endl;

    employee.setGrossSales(8000);       // set gross rate
    employee.setCommissionRate(0.1);    // set commission rate

    cout << "\nUpdated Employee information output by print function:\n" << endl;
    employee.print();

    // display the employee's earnings
    cout << "\n\nEmployee's earnings: $" << employee.earnings() << endl;

    return 0;
}
*/



//Fig. 12.16: fig12_16.cpp
#include <iostream>
using std::cout;
using std::endl;
using std::fixed;

#include <iomanip>
using std::setprecision;

#include "BasePlusCommissionEmployee.h"

int main()
{
    BasePlusCommissionEmployee employee("Bob", "Lewis", "333-33-3333", 5000, 0.04, 300);

    // set foating-point output formatting
    cout << fixed << setprecision(2);

    // get commission employee data
    cout << "Employee information obtained by get functions:\n"
         << "\nFirst name is " << employee.getFirstName()
         << "\nLast name is" << employee.getLastName()
         << "\nSocial security number is " << employee.getSocialSecurityNumber()
         << "\nGross sales is " << employee.getGrossSales()
         << "\nCommission rate is " << employee.getCommissionRate()
         << "\nBase salary rate is " << employee.getBaseSalary() << endl;

    // display the employee's earnings
    cout << "\n\nEmployee's earnings: $" << employee.earnings() << endl;

    employee.setBaseSalary(1000);       // set base salary
    employee.setCommissionRate(0.1);    // set commission rate

    cout << "\nUpdated Employee information output by print function:\n" << endl;
    employee.print();

    // display the employee's earnings
    cout << "\n\nEmployee's earnings: $" << employee.earnings() << endl;
    cout << "\n\nEmployee's earnings: $" << employee.CommissionEmployee::earnings() << endl;
    return 0;
}
