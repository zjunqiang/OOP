//使用虚基类，使派生类中只有基类的一个拷贝。

#include <iostream>
using namespace std;

class CFurniture
{
protected:
	int weight;
public:
	CFurniture(){}
	void SetWeight(int i){weight=i;}
	int GetWeight(){return weight;}
};
class CBed:virtual public CFurniture     //A   定义虚基类
{
public:
	CBed(){}
	void Sleep()
	{cout<<"Sleeping...\n";}
};

class CSofa:virtual public CFurniture    //B   定义虚基类
{
public:
	CSofa(){}
	void WatchTV()
	{ cout<<"Watch TV. \n";}
};

class CSleepSofa:public CBed,public CSofa
{
public:
	CSleepSofa(){}
	void FoldOut()
	{cout<<"Fold out the sofa.\n";}
};

int main()
{
	CSleepSofa ss;
	ss.SetWeight(20);
	cout<<ss.GetWeight()<<'\n';

	//ss.CBed::SetWeight(20);				//C
	//cout<<ss.CBed::GetWeight()<<'\n';		//D
	//cout<<ss.CSofa::GetWeight()<<'\n';		//E

	return 0;
}
