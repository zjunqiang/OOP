#ifndef PACKAGE_H_INCLUDED
#define PACKAGE_H_INCLUDED
#include <string>
using std::string;

class Package
{
public:
    Package();
    Package(const string &, const string &, const string &, const string &,
            const string &, const string &, const string &, const string &,
            double, double);

    double calculateCost();

private:
    string fromName;
    string fromAddr;
    string fromCity;
    string fromPostalCode;
    string toName;
    string toAddr;
    string toCity;
    string toPostalCode;

    double weight;
    double price;
};

#endif // PACKAGE_H_INCLUDED
