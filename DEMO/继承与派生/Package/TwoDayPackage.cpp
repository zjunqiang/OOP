#include "TwoDayPackage.h"


TwoDayPackage::TwoDayPackage(const string & fromName, const string & fromCity, const string & fromAddr, const string & fromPostalCode,
        const string & toName, const string & toCity, const string & toAddr, const string & toPostalCode,
        double weight, double price, double extraFee)
        :Package(fromName, fromCity, fromAddr, fromPostalCode,
        toName, toCity, toAddr, toPostalCode,
        weight, price), extraFee(extraFee)
{
}

double TwoDayPackage::calculateCost()
{
    return extraFee + Package::calculateCost();//weight * price;
}
