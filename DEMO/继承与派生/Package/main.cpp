#include <iostream>
using std::cout;
using std::endl;

#include "Package.h"
#include "TwoDayPackage.h"

int main()
{
    Package p1("zhangsan", "nanjing", "Nanjing Tech University", "211800",
               "Lisi", "Beijing", "Peking University", "100100",
               100, 1.23);
    cout << p1.calculateCost() << endl;

    TwoDayPackage p2("zhangsan", "nanjing", "Nanjing Tech University", "211800",
               "Lisi", "Beijing", "Peking University", "100100",
               100, 1.23, 50);
    cout << p2.calculateCost() << endl;
    return 0;
}
