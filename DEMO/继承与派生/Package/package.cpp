#include "Package.h"

Package::Package()
{

}

Package::Package(const string & fromName, const string & fromCity, const string & fromAddr, const string & fromPostalCode,
        const string & toName, const string & toCity, const string & toAddr, const string & toPostalCode,
        double weight, double price)
{
    this->fromName = fromName;
    this->fromCity = fromCity;
    this->fromAddr = fromAddr;
    this->fromPostalCode = fromPostalCode;

    this->fromName = fromName;
    this->toCity = toCity;
    this->toAddr = toAddr;
    this->toPostalCode = toPostalCode;

    this->weight = weight;
    this->price = price;
}

double Package::calculateCost()
{
    return weight * price;
}
