//虚函数与多态性

#include <iostream>
using namespace std;

//-----Shape类-----
class Shape
{
	int i;
public:
	Shape():i(7)  { }
	~Shape() { }
	virtual void Rotate() { cout << "将图旋转\n";}
	virtual void Erase() { cout << "将图清除\n"; }
};

//-----Circle类-----
class Circle : public Shape
{
	int r;
public:
	Circle(int n = 5) : r(n)  { }
	~Circle()  { }
	void Rotate() { cout << "将圆形旋转\n";}
	void Erase() { cout << "将圆形清除\n"; }
};

void Turn(Shape &s1)
{ s1.Rotate(); }

void Remove(Shape *ps)
{ ps->Erase(); }


//---主程序---
int main()
{
	Circle c1;
	Circle *pC1 = &c1;
	Turn(c1);
	Remove(pC1);

	return 0;
}