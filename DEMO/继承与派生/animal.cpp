#include <iostream>
using namespace std;

//静态多态性  编译时多态性 -- 函数重载

//动态多态性  运行时多态性 -- 基类指针指向派生类对象
//                            基类引用引用派生类对象

//non-virtual 不希望子类改变的函数
//virtual 希望子类可以重写的函数

//抽象类  如果有纯虚函数
class Animal
{
public:
	//虚函数 --> 纯虚函数
	virtual void eat() = 0;
};

class Tiger:public Animal
{
public:
	//虚函数
	virtual void eat()
	{
		cout << "eatting meat." << endl;
	}
};

class Sheep:public Animal
{
public:
	//虚函数
	virtual void eat()
	{
		cout << "eatting grass." << endl;
	}
};

int main()
{
	//Animal a;		//基类对象a
	Animal *pa;		//基类指针pa

	Tiger t;		//派生类对象t
	Sheep s;		//派生类对象s

	pa = &t;		//基类指针指向派生类对象
	pa->eat();

	pa = &s;		//基类指针指向派生类对象
	pa->eat();

	/*
	a = t;
	//t = a;
	a.eat();
*/
	return 0;
}
