//使用虚基类，使派生类中只有基类的一个拷贝。

#include <iostream>
#include <string>
using namespace std;

class A
{
public:
	A(const string & s) { cout << s << endl; }
	~A() { }
};

class B : virtual public A
{
public:
	B(const string & s1, const string & s2):A(s1)
	{  cout << s2 << endl;  }
};

class C: virtual public A
{
public:
	C(const string & s1, const string & s2):A(s1)
	{ cout << s2 << endl; }
};

class D: public B, public C
{
public:
	D(const string & s1, const string & s2, const string & s3, const string & s4):B(s1,s2),C(s1,s3),A(s1)
	{ cout << s4 << endl; }
};


int main()
{
	D *p = new D("class A","class B", "class C", "class D");
	delete p;

	return 0;
}
