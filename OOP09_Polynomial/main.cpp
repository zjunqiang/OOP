/*
教材480页多项式类11.17
201821017201张旭
*/

#include "Polynomial.h"
#include <iostream>

using std::cout;
using std::endl;
using std::boolalpha;

int main()
{
    double a[6] = { 1, 2.5, -2, 3, 0, 5 };
    double b[2] = { 1,1 };
    double c[11] = {1, 0, 0, 0, 0, 0, 3, 0, 0, 0, 8};
    Polynomial p1;
    Polynomial p2(a, 3);
    Polynomial p3(a, 5);
    Polynomial p4(a, 2);
    Polynomial p5(p4);
    Polynomial p6(b, 1);
    Polynomial p7(c, 10);
    Polynomial p8(c, 20);               // bug

    cout << "p1(x) = " << p1 << endl;
    cout << "p2(x) = " << p2 << endl;
    cout << "p3(x) = " << p3 << endl;
    cout << "p4(x) = " << p4 << endl;
    cout << "p5(x) = " << p5 << endl;
    cout << "p6(x) = " << p6 << endl;
    cout << "p7(x) = " << p7 << endl;
    cout << "p8(x) = " << p8 << endl;

    cout << "\n接下来演示一些判断:" << endl;
    cout << "!p1(x) : " << boolalpha << (!p1) << endl;
    cout << "!p2(x) : " << boolalpha << (!p2) << endl;

    cout << "\n接下来演示一些修改:" << endl;
    cout << "修改前···········: p4(x) = " << p4 << endl;
    p4.modify(1, -2);
    cout << "执行代码\"p4.modify(1, -2)\"后: p4(x) = " << p4 << endl;
    p4.modify(8, 5);
    cout << "执行代码\"p4.modify(8, 5)\" 后: p4(x) = " << p4 << endl;

    cout << "\n接下来演示一些读取:" << endl;
    cout << "p3[5] = " << p3[5] << endl;
    cout << "p3[2] = " << p3[2] << endl;
    cout << "p3[9] = " << p3[9] << endl;

    cout << "\n接下来演示一些计算:" << endl;
    cout << "-p2(x) = " << -p2 << endl;
    cout << "-p1(x) = " << -p1 << endl;
    cout << "p2(1) = " << p2(1) << endl;
    cout << "p3(2) = " << p3(2) << endl;
    cout << "p2 + p3 = " << p2 + p3 << endl;
    cout << "p3 - p5 = " << p3 - p5 << endl;
    cout << "p2 * p5 = " << p2 * p5 << endl;
    cout << "p6 * p6 = " << p6 * p6 << endl;
    cout << "修改前 ·······: p4(x) = " << p4 << endl;
    p4 += p2;
    cout << "执行代码\"p4 += p2\"后:  p4(x) = " << p4 << endl;
    p4 += p1;
    cout << "执行代码\"p4 += p1\" 后: p4(x) = " << p4 << endl;
    p4 -= p3;
    cout << "执行代码\"p4 -= p3\" 后: p4(x) = " << p4 << endl;
    cout << "修改前 ·······: p6(x) = " << p6 << endl;
    p6 *= p6;
    cout << "执行代码\"p6 *= p6\" 后: p6(x) = " << p6 << endl;

    return 0;
}
