#include "Polynomial.h"
#include <iostream>
#include <cmath>

using std::cerr;
using std::endl;

Polynomial::Polynomial()
{
    termNumber = 0;
    ptr = new double[1];
    *ptr = 0;
}

Polynomial::Polynomial(const double* p, int n)
{
    //去除非法最高次数(去除负数最高次数)
    n = n >= 0 ? n : 0;
    //非空指针
    if (p)
    {
        termNumber = n;
        ptr = new double[termNumber + 1];
        for (int i = 0; i <= n; ++i)
        {
            ptr[i] = p[i];
        }
    }
    //空指针
    else
    {
        termNumber = 0;
        ptr = new double[1];
        *ptr = 0;
    }
}

Polynomial::Polynomial(const Polynomial& _p)
{
    termNumber = _p.termNumber;
    ptr = new double[termNumber + 1];
    for (int i = 0; i < termNumber + 1; ++i)
    {
        ptr[i] = _p.ptr[i];
    }
}

Polynomial::~Polynomial()
{
    if (ptr)
    {
        delete[]ptr;
    }
}

void Polynomial::modify(int n, double elem)
{
    //多项式次数非法
    if (n < 0)
    {
        cerr << "多项式次数（" << n << "）非法!多项式最低次数为0（常数项）" << endl;
        exit(1);
    }
    //修改的系数为存在的合法低次系数
    if (termNumber >= n)
    {
        ptr[n] = elem;
    }
    //修改的系数为不存在的合法高次系数
    else
    {
        //先备份原来系数，并且录入新的系数
        double* temp = new double[n + 1];
        int i = 0;
        for (; i <= termNumber; ++i)
        {
            temp[i] = ptr[i];
        }
        for (; i < n; ++i)
        {
            temp[i] = 0;
        }
        temp[i] = elem;
        //录入对象新数据
        termNumber = n;
        delete[]ptr;
        ptr = temp;
    }
}

double Polynomial::operator[](int n)const
{
    //多项式系数非法
    if (n < 0)
    {
        cerr << "多项式次数（" << n << "）非法!多项式最低次数为0（常数项）" << endl;
        exit(1);
    }
    //读取的系数为存在的合法低次系数
    if (termNumber >= n)
        return ptr[n];
    //读取的系数为不存在的合法高次系数
    return 0;
}

bool Polynomial::operator!()const
{
    for (int i = 0; i <= termNumber; ++i)
    {
        if (ptr[i])
            return false;
    }
    return true;
}

Polynomial Polynomial::operator-()const
{
    Polynomial temp = *this;
    for (int i = 0; i <= temp.termNumber; ++i)
    {
        temp.ptr[i] *= -1;
    }
    return temp;
}

double Polynomial::operator()(double n)const
{
    double result = 0;
    for (int i = 0; i <= termNumber; ++i)
    {
        result += ptr[i] * pow(n, i);
    }
    return result;
}

Polynomial Polynomial::operator+(const Polynomial& rhs)const
{
    //调节临时对象的最高次数
    int N = termNumber >= rhs.termNumber ? termNumber : rhs.termNumber;
    double *a = new double[N + 1];
    //计算并返回
    if (termNumber >= rhs.termNumber)
    {
        //为了不修改原来的对象暂时定义一个对象来代替其进行修改数据简化计算
        Polynomial tempFor_rhs = rhs;
        //调节次数
        tempFor_rhs.modify(N, 0);
        //计算
        for (int i = 0; i <= N; ++i)
        {
            a[i] = ptr[i] + tempFor_rhs.ptr[i];
        }
    }
    else
    {
        //为了不修改原来的对象暂时定义一个对象来代替其进行修改数据简化计算
        Polynomial tempFor_this = *this;
        //调节次数
        tempFor_this.modify(N, 0);
        //计算
        for (int i = 0; i <= N; ++i)
        {
            a[i] = tempFor_this.ptr[i] + rhs.ptr[i];
        }
    }
    Polynomial temp(a, N);
    delete[]a;
    return temp;
}

Polynomial Polynomial::operator-(const Polynomial& rhs)const
{
    return *this + (-rhs);
}

Polynomial Polynomial::operator*(const Polynomial& rhs)const
{
    int N = termNumber + rhs.termNumber;
    double *a = new double[N + 1];
    for (int i = 0; i <= N; ++i)
    {
        a[i] = 0;
    }
    //计算并返回
    //为了不修改原来的对象暂时定义一个对象来代替其进行修改数据简化计算
    Polynomial tempFor_rhs = rhs;
    Polynomial tempFor_this = *this;
    //调节次数
    tempFor_rhs.modify(N, 0);
    tempFor_this.modify(N, 0);
    //计算
    for (int i = 0; i <= N; ++i)
    {
        for (int j = 0; j <= i; ++j)
        {
            a[i] += tempFor_this.ptr[j] * tempFor_rhs.ptr[i - j];
        }
    }
    Polynomial temp(a, N);
    delete[]a;
    return temp;
}

Polynomial& Polynomial::operator=(const Polynomial& rhs)
{
    if (this != &rhs)
    {
        termNumber = rhs.termNumber;
        delete[]ptr;
        ptr = new double[termNumber + 1];
        for (int i = 0; i <= termNumber; ++i)
        {
            ptr[i] = rhs.ptr[i];
        }
    }
    return *this;
}

Polynomial& Polynomial::operator+=(const Polynomial& rhs)
{
    *this = *this + rhs;
    return *this;
}

Polynomial& Polynomial::operator-=(const Polynomial& rhs)
{
    *this = *this - rhs;
    return *this;
}

Polynomial& Polynomial::operator*=(const Polynomial& rhs)
{
    *this = *this * rhs;
    return *this;
}

ostream& operator<<(ostream& out, const Polynomial& _p)
{
    //如果多项式为0
    if (!_p)
        out << 0;
    //如果多项式非0
    else
    {
        out << _p.ptr[0];
        for (int i = 1; i <= _p.termNumber; ++i)
        {
            if (_p.ptr[i] > 0)
                out << " + " << _p.ptr[i] << "(x^" << i << ')';
            else if (_p.ptr[i] == 0)
                continue;
            else
                out << " - " << -_p.ptr[i] << "(x^" << i << ')';
        }
    }
    return out;
}
