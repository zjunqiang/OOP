#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <iostream>

using std::ostream;

class Polynomial
{
    friend ostream& operator<<(ostream&, const Polynomial&);
public:
    /*构造函数*/
    Polynomial();
    Polynomial(const double*, int);
    Polynomial(const Polynomial&);

    /*析构函数*/
    ~Polynomial();

    /*设置函数*/
    void modify(int, double);

    /*获取运算符*/
    double operator[](int)const;//(因为这里我是认同了了超出对象的指针范围存在的高次0系数合法可以作为系数，所以不适合重载返回引用的[]运算符)

    /*辅助运算符*/
    bool operator!()const;			//判断多项式是否为0
    Polynomial operator-()const;//负号运算符

    /*应用于给多项式赋值的括号()运算符*/
    double operator()(double)const;

    /*算术运算符*/
    Polynomial operator+(const Polynomial&)const;
    Polynomial operator-(const Polynomial&)const;
    Polynomial operator*(const Polynomial&)const;
    Polynomial& operator=(const Polynomial&);
    Polynomial& operator+=(const Polynomial&);
    Polynomial& operator-=(const Polynomial&);
    Polynomial& operator*=(const Polynomial&);
private:
    double *ptr;   //多项式系数指针
    int termNumber;//多项式最高次数
};

#endif // !POLYNOMIAL_H
