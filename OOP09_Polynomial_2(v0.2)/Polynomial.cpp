#include "polynomial.h"
Polynomial::Polynomial()
{
	this->terms = 0;
	this->size = 10;
	termArray = new Term[this->size];
}

Polynomial::Polynomial(const Polynomial & b)
{
	this->terms = 0;
	this->size = b.size;
	termArray = new Term[this->size];
	for (int i = 0; i < b.terms; i++)
	{
		NewTerm(b.termArray[i].coef, b.termArray[i].exp);
	}
}

Polynomial::~Polynomial()
{
	delete[] termArray;
}

Polynomial Polynomial::operator+(const Polynomial & b)
{
	Polynomial c;
	int aPos = 0;
	int bPos = 0;
	while (aPos < terms && bPos < b.terms)
	{
		if (termArray[aPos].exp == b.termArray[bPos].exp)//次数相同
		{
			float coef = termArray[aPos].coef + b.termArray[bPos].coef;//系数相加
			if (coef)//系数不为零
				c.NewTerm(coef, termArray[aPos].exp);//添加一项
			aPos++; bPos++;
		}
		//次数不相同，分类讨论
		else if (termArray[bPos].exp < b.termArray[bPos].exp)
		{
			c.NewTerm(b.termArray[bPos].coef, b.termArray[bPos].exp);
			bPos++;
		}
		else
		{
			c.NewTerm(termArray[aPos].coef, termArray[aPos].exp);
			aPos++;
		}
	}
	while (aPos < terms)
	{
		c.NewTerm(termArray[aPos].coef, termArray[aPos].exp);
		aPos++;
	}
	while (bPos < b.terms)
	{
		c.NewTerm(b.termArray[bPos].coef, b.termArray[bPos].exp);
		bPos++;
	}
	return c;
}
Polynomial Polynomial::operator-(const Polynomial& b)
{
	Polynomial c;
	int aPos = 0;
	int bPos = 0;
	while (aPos < terms && bPos < b.terms)
	{
		if (termArray[aPos].exp == b.termArray[bPos].exp)//次数相同
		{
			float coef = termArray[aPos].coef - b.termArray[bPos].coef;//系数相减
			if (coef)//系数不为零
				c.NewTerm(coef, termArray[aPos].exp);//添加一项
			aPos++; bPos++;
		}
		//次数不相同，分类讨论
		else if (termArray[bPos].exp < b.termArray[bPos].exp)
		{
			c.NewTerm(b.termArray[bPos].coef * -1, b.termArray[bPos].exp);
			bPos++;
		}
		else
		{
			c.NewTerm(termArray[aPos].coef, termArray[aPos].exp);
			aPos++;
		}
	}
	while (aPos < terms)
	{
		c.NewTerm(termArray[aPos].coef, termArray[aPos].exp);
		aPos++;
	}
	while (bPos < b.terms)
	{
		c.NewTerm(b.termArray[bPos].coef * -1, b.termArray[bPos].exp);
		bPos++;
	}
	return c;
}

Polynomial Polynomial::operator*(const Polynomial & b)
{
	Polynomial c;
	for (int i = 0; i < terms; i++)
	{
		for (int j = 0; j < b.terms; j++)
		{
			float coef = termArray[i].coef * b.termArray[j].coef;
			int exp = termArray[i].exp + b.termArray[j].exp;
			c.NewTerm(coef, exp);
		}
	}
	return c;
}

Polynomial Polynomial::operator+=(const Polynomial & b)
{
	int aPos = 0;
	int bPos = 0;
	while (aPos < terms && bPos < b.terms)
	{
		if (termArray[aPos].exp == b.termArray[bPos].exp)//次数相同
		{
			float coef = b.termArray[bPos].coef;
			if (coef)//系数不为零
				NewTerm(coef, termArray[aPos].exp);//添加一项
			aPos++; bPos++;
		}
		//次数不相同，分类讨论
		else if (termArray[bPos].exp < b.termArray[bPos].exp)
		{
			NewTerm(b.termArray[bPos].coef, b.termArray[bPos].exp);
			bPos++;
		}
		else
		{
			NewTerm(termArray[aPos].coef, termArray[aPos].exp);
			aPos++;
		}
	}
	while (aPos < terms)
	{
		NewTerm(termArray[aPos].coef, termArray[aPos].exp);
		aPos++;
	}
	while (bPos < b.terms)
	{
		NewTerm(b.termArray[bPos].coef, b.termArray[bPos].exp);
		bPos++;
	}
	return *this;
}

Polynomial Polynomial::operator-=(const Polynomial & b)
{
	int aPos = 0;
	int bPos = 0;
	while (aPos < terms && bPos < b.terms)
	{
		if (termArray[aPos].exp == b.termArray[bPos].exp)//次数相同
		{
			float coef = - b.termArray[bPos].coef;
			if (coef)//系数不为零
				NewTerm(coef, termArray[aPos].exp);//添加一项
			aPos++; bPos++;
		}
		//次数不相同，分类讨论
		else if (termArray[bPos].exp < b.termArray[bPos].exp)
		{
			NewTerm(b.termArray[bPos].coef * -1, b.termArray[bPos].exp);
			bPos++;
		}
		else
		{
			NewTerm(termArray[aPos].coef, termArray[aPos].exp);
			aPos++;
		}
	}
	while (aPos < terms)
	{
		NewTerm(termArray[aPos].coef, termArray[aPos].exp);
		aPos++;
	}
	while (bPos < b.terms)
	{
		NewTerm(b.termArray[bPos].coef, b.termArray[bPos].exp);
		bPos++;
	}
	return *this;
}

Polynomial Polynomial::operator*=(const Polynomial & b)
{
	for (int i = 0; i < terms; i++)
	{
		for (int j = 0; j < b.terms; j++)
		{
			float coef = termArray[i].coef * b.termArray[j].coef;
			int exp = termArray[i].exp + b.termArray[j].exp;
			NewTerm(coef, exp);
		}
	}
	return *this;
}

/*
Polynomial Polynomial::operator=(const Polynomial& b)
{
	this->terms = b.terms;
	this->size = b.size;
	termArray = new Term[this->size];
	for (int i = 0; i < b.terms; i++)
	{
		NewTerm(b.termArray[i].coef, b.termArray[i].exp);
	}
	return *this;
}*/


Polynomial& Polynomial::operator=(const Polynomial& b)
{
    if(&b == this)
        return *this;

	this->terms = b.terms;
	this->size = b.size;
	delete []termArray;
	termArray = new Term[this->size];
	for (int i = 0; i < b.terms; i++)
	{
	    this->termArray[i].coef = b.termArray[i].coef;
	    this->termArray[i].exp = b.termArray[i].exp;
	}
	return *this;
}

void Polynomial::NewTerm(float coef, int exp)
{
	if (terms == size)
	{
		size *= 2;
		Term * tmp = new Term[size];
		copy(termArray, termArray + terms, tmp);
		delete[] termArray;
		termArray = tmp;
	}
	Term ATerm;
	ATerm.coef = coef; ATerm.exp = exp;
	insertTerm(ATerm);
}
void Polynomial::insertTerm(const Term & term)
{
	int i;
	for (i = 0; i < terms && term.exp < termArray[i].exp; i++)
	{
	}
	//if (term.exp == termArray[i].exp)
    if (term.exp == termArray[i].exp && i < terms)
	{
		termArray[i].coef += term.coef;
		if (!termArray[i].coef)
		{
			for (int j = i; j < terms - 1; j++)
				termArray[j] = termArray[j + 1];
			terms--;
		}
	}
	else
	{
		for (int j = terms - 1; j >= i; j--)
			termArray[j + 1] = termArray[j];
		termArray[i] = term;
		terms++;
	}
}

ostream & operator<<(ostream & output, const Polynomial & poly)
{
	for (int i = 0; i < poly.terms - 1; i++)
	{
		output << poly.termArray[i].coef << "x^" << poly.termArray[i].exp << " + ";
	}
	output << poly.termArray[poly.terms - 1].coef << "x^" << poly.termArray[poly.terms - 1].exp;
	return output;
}
