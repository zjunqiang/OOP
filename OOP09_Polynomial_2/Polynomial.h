#pragma once
#include <iostream>
#include<algorithm>
using  std::ostream;
using std::copy;

class Polynomial;
class Term
{//多项式的每一项
	friend Polynomial;
public:
	float coef;//系数
	int exp;//指数
};

class Polynomial
{//多项式类
	friend ostream & operator<<(ostream &o, const Polynomial & poly);
public:
	Polynomial();
	Polynomial(const Polynomial & poly);
	~Polynomial();
	Polynomial operator+(const Polynomial & poly);//多项式加法
	Polynomial operator*(const Polynomial & poly);//多项式乘法
	Polynomial operator-(const Polynomial & poly);//多项式减法
	Polynomial operator+=(const Polynomial & poly);
	Polynomial operator*=(const Polynomial & poly);
	Polynomial operator-=(const Polynomial & poly);
	Polynomial operator=(const Polynomial & poly);//赋值运算符
	void NewTerm(float coef, int exp);//添加一项,若有相同的指数项，则合并
private:
	void insertTerm(const Term & term);//项的有序插入
private:
	Term *termArray;//储存每一项的数组
	int size;//储存每一项的数组大小
	int terms;//总的项数
};
