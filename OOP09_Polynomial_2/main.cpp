/*
201821017203������
*/

//#include "pch.h"
#include <iostream>
#include "polynomial.h"
using std::cout;
using std::endl;

int main()
{
	Polynomial p1;
	p1.NewTerm(3, 2);
	p1.NewTerm(2.1, 3);
	p1.NewTerm(10, 1);      // test

	Polynomial p2;
	p2.NewTerm(1, 2);
	p2.NewTerm(1, 3);
	p2.NewTerm(5, 1);

	cout << "p1:\n" << p1 << "\np2:\n" << p2 << endl;
	cout << "(" << p1 << ") + (" << p2 << ") = " << p1 + p2 << endl;
	cout << "(" << p1 << ") * (" << p2 << ") = " << p1 * p2 << endl;
	cout << "(" << p1 << ") - (" << p2 << ") = " << p1 - p2 << endl;
	p1 += p2;
	cout << "After p1 += p2\n" << "p1:\n" << p1 << endl;
	p1 -= p2;
	cout << "After p1 -= p2\n" << "p1:\n" << p1 << endl;

	// test
	p1 = p2;
	cout << "After p1 = p2\n" << "p1:\n" << p1 << endl;
	cout << "p2:\n" << p2 << endl;
}

