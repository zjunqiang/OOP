#include "Polynomial.h"
#include <iostream>
using std::istream;
using std::ostream;
using std::cout;
using std::endl;
#define N 3

Polynomial::Polynomial()
{
	s = new int[N];
	for (int i = 0; i < N; i++)
	{
		s[i] = 0;
	}
	//cout << "����" << s << endl;
}

Polynomial::Polynomial(const Polynomial&p)
{
	s = new int[N];
	for (int i = 0; i < N; i++)
	{
		s[i] = p.s[i];
	}
}

Polynomial::~Polynomial()
{
	//cout << "����" << s << endl;
	delete[]s;
}

Polynomial& Polynomial::operator=(const Polynomial&p)
{
	if(this == &p)
		return *this;

	for (int i = 0; i < N; i++)
	{
		s[i] = p.s[i];
	}

	return *this;
}
Polynomial Polynomial::operator+(const Polynomial&p)
{
	Polynomial t;
	//cout << t << endl;
	for (int i = 0; i < N; i++)
	{
		t.s[i] = this->s[i] + p.s[i];
	}

	return t;
}

ostream& operator<<( ostream&output, const Polynomial&p)
{
	for (int i = 0; i < N; i++)
	{
		if (p.s[i] != 0)
		{
			if (i == 0)
				output << p.s[i];
			else if (p.s[i] > 0 && p.s[i] != 1)
				output << "+" << p.s[i] << "x^" << i;
			else if (p.s[i] > 0 && p.s[i] == 1)
				output << "+x^" << i;
			else if (p.s[i] < 0 && p.s[i] != -1)
				output << p.s[i] << "x^" << i;
			else if (p.s[i] < 0 && p.s[i] == -1)
				output << "-x^" << i;
		}
	}

	int i ;
	for ( i = 0; i < N; i++)
	{
		if (p.s[i] != 0)
			break;
	}

	if (i == N)
		cout << 0 << endl;

	return output;
}
istream& operator>>(istream& input, Polynomial& p)
{
	for (int i = 0; i < N; i++)
	{
		input >> p.s[i];
	}

	return input;
}
