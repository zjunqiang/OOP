#ifndef POLYNOMIAL_H_INCLUDED
#define POLYNOMIAL_H_INCLUDED
#include <iostream>
using std::ostream;
using std::istream;
class Polynomial
{
	friend ostream& operator<<(ostream&, const Polynomial&);
	friend istream& operator>>(istream&, Polynomial&);
public:
	Polynomial();
	Polynomial(const Polynomial&);
	~Polynomial();

	Polynomial& operator=(const Polynomial&);
	Polynomial operator+(const Polynomial&);

private:
	int* s;
};


#endif // POLYNOMIAL_H_INCLUDED
