#include "Circle.h"
#include <iostream>
#define PI 3.1415926

Circle::Circle(double r):radius(r)
{

}

void Circle::draw()
{
    std::cout << "Circle(" << radius << ")" << std::endl;
}

double Circle::getArea() const
{
    return PI * radius * radius;
}
