#ifndef CIRCLE_H_INCLUDED
#define CIRCLE_H_INCLUDED

#include "TwoDimentioanlShape.h"

class Circle:public TwoDimentioanlShape
{
public:
    Circle(double);
    virtual void draw();
    virtual double getArea() const;

    /*
    virtual ~Circle()
    {
        std::cout << "dtor Circle\n";
    }*/
private:
    double radius;
};


#endif // CIRCLE_H_INCLUDED
