#include "Cone.h"
#include <iostream>
#include <cmath>
#define PI 3.1415926

Cone::Cone(double r, double h):radius(r), height(h)
{

}

void Cone::draw()
{
    std::cout << "Cone(" << radius << ", " << height << ")" << std::endl;
}

double Cone::getArea() const
{
    return PI * radius * radius * height / 3;
}

double Cone::getVolume() const
{
    return PI * radius * (radius + sqrt(height * height + radius * radius)) ;
}
