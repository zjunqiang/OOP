#ifndef CONE_H_INCLUDED
#define CONE_H_INCLUDED
#include "TwoDimentioanlShape.h"

#include "ThreeDimentioanlShape.h"

class Cone:public ThreeDimentioanlShape
{
public:
    Cone(double, double);
    virtual void draw();
    virtual double getArea() const;
    virtual double getVolume() const;
private:
    double radius;
    double height;
};

#endif // CONE_H_INCLUDED
