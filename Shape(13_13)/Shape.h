#ifndef SHAPE_H_INCLUDED
#define SHAPE_H_INCLUDED
#include <iostream>

class Shape
{
public:
    virtual void draw() = 0;
    virtual ~Shape()
    {
        //std::cout << "dtor Shape\n";
    }
};

#endif // SHAPE_H_INCLUDED
