#ifndef THREEDIMENTIOANLSHAPE_H_INCLUDED
#define THREEDIMENTIOANLSHAPE_H_INCLUDED

#include "Shape.h"

class ThreeDimentioanlShape:public Shape
{
public:
    virtual void draw();
    virtual double getArea() const = 0;
    virtual double getVolume() const = 0;
};



#endif // THREEDIMENTIOANLSHAPE_H_INCLUDED
