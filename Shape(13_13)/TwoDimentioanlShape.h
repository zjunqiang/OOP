#ifndef TWODIMENTIOANLSHAPE_H_INCLUDED
#define TWODIMENTIOANLSHAPE_H_INCLUDED

#include "Shape.h"

class TwoDimentioanlShape:public Shape
{
public:
    virtual void draw();
    virtual double getArea() const = 0;

    /*
    virtual ~TwoDimentioanlShape()
    {
        std::cout << "dtor TwoDimentioanlShape\n";
    }*/
};


#endif // TWODIMENTIOANLSHAPE_H_INCLUDED
