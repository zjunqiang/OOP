#include <iostream>
#include <vector>
#include <typeinfo>

using namespace std;
#include "Shape.h"
#include "TwoDimentioanlShape.h"
#include "Circle.h"
#include "Cone.h"

int main()
{
    vector<Shape*> v(4);
    //Shape * v[2];

    v[0] = new Circle(10);
    v[1] = new Cone(10, 20);
    v[2] = new Cone(30, 40);
    v[3] = new Circle(20);

    for(size_t i = 0; i < v.size(); ++i)
    {
        v[i]->draw();

        TwoDimentioanlShape *two = dynamic_cast<TwoDimentioanlShape*>(v[i]);
        if(two != 0)
        {
             cout << "area :" << two->getArea() << endl << endl;
        }

        ThreeDimentioanlShape *three = dynamic_cast<ThreeDimentioanlShape*>(v[i]);
        if(three != 0)
        {
             cout << "area :" << three->getArea() << endl;
             cout << "volume :" << three->getVolume() << endl << endl;
        }
    }

    for(size_t i = 0; i < v.size(); ++i)
    {
        cout << "deleting object of " << typeid(*v[i]).name() << endl;
        delete v[i];
    }


    return 0;
}
