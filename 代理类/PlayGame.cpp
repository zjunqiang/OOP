//游戏中，通过代理来控制不同vip玩家的游戏权限。
#include <iostream>

using namespace std;

//基本操作接口
class Play
{
public:
    virtual void Play1() = 0;
    virtual void Play2() = 0;
    virtual void Play3() = 0;
};

//操作类：
class Player:public Play
{
public:
    void Play1()
    {
        cout<<"战役"<<endl;
    }
    void Play2()
    {
        cout<<"军团"<<endl;
    }
    void Play3()
    {
        cout<<"神器"<<endl;
    }

};

//不同vip玩家的代理：
class ProxyPlayerVip0:Play
{
public:
    ProxyPlayerVip0()
    {
        mPlayer = new Player;
    }
    void Play1()
    {
        mPlayer->Play1();
    }

    void Play2()
    {
        cout<<"没有权限"<<endl;
    }


    void Play3()
    {
        cout<<"没有权限"<<endl;
    }

private:
    Play* mPlayer;
};

class ProxyPlayerVip1:Play
{
public:
    ProxyPlayerVip1()
    {
        mPlayer = new Player;
    }
    void Play1()
    {
        mPlayer->Play1();
    }

    void Play2()
    {
        mPlayer->Play2();
    }

    void Play3()
    {
        cout<<"没有权限"<<endl;
    }

private:
    Play* mPlayer;
};




int main()
{
    cout << "Player Vip0 " << endl;
    ProxyPlayerVip0 pro5;
    pro5.Play1();
    pro5.Play2();
    pro5.Play3();

    cout << "\nPlayer Vip1 " << endl;
    ProxyPlayerVip1 pro1;
    pro1.Play1();
    pro1.Play2();
    pro1.Play3();

    return 0;
}
