//Implementation.h
#ifndef IMPLEMENTATION_H
#define IMPLEMENTATION_H
class Implementation
{
public:
	Implementation(int v):value(v)
	{
	}

	void setValue(int v)
	{
		value = v;
	}

	int getValut() const
	{
		return value;
	}
private:
	int value;
};
#endif



class Implementation;
//#include "Implementation.h"


class Interface
{
public:
	Interface(int);
	void setValue(int);
	int getVaule() const;
	~Interface();
private:
	Implementation *ptr;
};



#include "Interface.h"
#include "Implementation.h"

Interface::Interface(int v):ptr(new Implementation(v))
{
}

void Interface::setValue(int v)
{
	ptr->setValue(v);
}

int Interface::getVaule() const
{
	return ptr->getValut();
}

Interface::~Interface()
{
	delete ptr;
}



#include <iostream>
using std::cout;
using std::endl;

#include "Interface.h"

int main()
{
	Interface i(5);
	cout << i.getVaule() << endl;

	i.setValue(10);

	cout << i.getVaule() << endl;

	return 0;
}

