#include <fstream>
#include <iostream>
using namespace std;

//将一个整数、一个字符串、一个double依次写到磁盘文件test1.txt
//再从文件中读取到变量并显示到屏幕
int main()
{

	// --- 写文件 ---
	ofstream outfile("test1.txt", ios::out);		//ios::app

	if(!outfile)
	{
		cout << "cannot open file" << endl;
		exit(1);
	}

	outfile << 12345 << " " << "hello" << " " << 1.23 << endl;

	outfile.close();
	
	cout << "writting file done..." << endl;

	
	int a;
	char b[30];
	double c;

	// --- 读文件 ---
	ifstream infile("test1.txt", ios::in);		//ios::app

	if(!infile)
	{
		cout << "cannot open file" << endl;
		exit(1);
	}

	infile >> a;			//从文件读取数据
	infile >> b;
	infile >> c;

	cout << a << " " << b << " " << c << endl; 

	infile.close();
	
	cout << "reading file done..." << endl;
	return 0;
}

