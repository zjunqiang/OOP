// position in output stream
#include <fstream>      // std::ofstream

/*In this example, tellp is used to get the position in the stream after the writing operation. 
The pointer is then moved back 7 characters to modify the file at that position, 
so the final content of the file is:
This is a sample
*/
int main () {

  std::ofstream outfile;
  outfile.open ("test.txt");

  outfile.write ("This is an apple",16);
  long pos = outfile.tellp();
  outfile.seekp (pos-7);
  outfile.write (" sam",4);

  outfile.close();

  return 0;
}